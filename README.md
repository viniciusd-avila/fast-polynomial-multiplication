# Fast Polynomial Multiplication
Implementation of FFT in Common Lisp to attain fast multiplication of polynomials, that is, in O(n log n)
